import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    selectedPokemon: null,
    lineup: [],
    action: 'default',
  },

  getters: {
    hasPokemon: state => {
      return Object.keys(state.pokemon).length > 0
    },

    lineup: state => {
      return state.lineup
    }
  },

  mutations: {
    'ADD_TO_LINEUP': (state, pokemon) => {
      state.lineup.push(pokemon)
    },

    'UPDATE_LINEUP': (state, lineup) => {
      state.lineup = lineup
    },

    'SET_CURRENT_POKEMON': (state, pokemon) => {
      state.selectedPokemon = pokemon
    },

    'REMOVE_CURRENT_POKEMON': (state) => {
      state.selectedPokemon = null
    },

    'SET_ACTION': (state, action) => {
      state.action = action
    }
  },
})
